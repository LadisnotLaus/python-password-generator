import generated_password

import os

def writing(generated_password):

    path = '/home/Lad/Chest/Passwords/'
    os.chdir(path)

    cycle = True

    with open('passwords.txt','a') as file:
        while cycle:
            decision = input('Do you wish to save this password?(y / n) (enter "r" to abandon)')
            if decision == 'y':
                cycle = False
                site_name = str(input('What is the name of the place you will use this password for?'))

                file.write("-"*20 + "\n""You are using this password at:" + site_name + "\n"
                           "Your password is:" + ''.join(str(generated_password.generated_password)) + '\n')


                if site_name == 'r':
                    cycle = False
                    break

            elif decision == 'n':
                cycle = False
                break

writing(generated_password)